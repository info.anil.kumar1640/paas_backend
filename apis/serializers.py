from rest_framework import serializers
from db.models import *


class OrganisationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = '__all__'

class AppSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppDetail
        fields = '__all__'

class DatabaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Database
        fields = '__all__'


class KeySerializer(serializers.ModelSerializer):
    class Meta:
        model = EnvDetail
        fields = '__all__'

