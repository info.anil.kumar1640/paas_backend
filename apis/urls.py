from django.urls import path
from db import views,models
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('list/organisations',OrganisationView.as_view(), name='organisation'),
    path('list/apps',AppView.as_view(), name='app'),
    path('list/databases',DatabaseView.as_view(), name='database'),
    path('list/keys',KeyView.as_view(), name='key'),

]
