from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView, ListCreateAPIView
# Create your views here.
from django.conf import settings
from django.shortcuts import get_object_or_404, redirect, render
from rest_framework import viewsets
from rest_framework.serializers import Serializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import GenericAPIView, ListCreateAPIView
from rest_framework import status, permissions
from .serializers import *

class OrganisationView(GenericAPIView):
    serializer_class = OrganisationSerializer
    queryset = Organization.objects.all()  # Define the queryset directly

    def get(self, request):
        try:
        
            queryset = self.get_queryset()
            serializer = self.get_serializer(queryset, many=True)
            data = {
                "is_success": True,
                "message": "Details",
                "data": serializer.data
            }
            return Response(data, status=status.HTTP_200_OK)
        
        except Exception as e:
            data = {
                "is_success": False,
                "error_message": str(e)
            }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
        
class AppView(GenericAPIView):
    serializer_class = AppSerializer
    queryset = AppDetail.objects.all()  # Define the queryset directly

    def get(self, request):
        try:
        
            queryset = self.get_queryset()
            serializer = self.get_serializer(queryset, many=True)
            data = {
                "is_success": True,
                "message": "App Details",
                "data": serializer.data
            }
            return Response(data, status=status.HTTP_200_OK)
        
        except Exception as e:
            data = {
                "is_success": False,
                "error_message": str(e)
            }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
        
class DatabaseView(GenericAPIView):
    serializer_class = DatabaseSerializer
    queryset = Database.objects.all()  # Define the queryset directly

    def get(self, request):
        try:
        
            queryset = self.get_queryset()
            serializer = self.get_serializer(queryset, many=True)
            data = {
                "is_success": True,
                "message": "Database Details",
                "data": serializer.data
            }
            return Response(data, status=status.HTTP_200_OK)
        
        except Exception as e:
            data = {
                "is_success": False,
                "error_message": str(e)
            }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

class KeyView(GenericAPIView):
    serializer_class = KeySerializer
    queryset = EnvDetail.objects.all()  # Define the queryset directly

    def get(self, request):
        try:
        
            queryset = self.get_queryset()
            serializer = self.get_serializer(queryset, many=True)
            data = {
                "is_success": True,
                "message": "Database Details",
                "data": serializer.data
            }
            return Response(data, status=status.HTTP_200_OK)
        
        except Exception as e:
            data = {
                "is_success": False,
                "error_message": str(e)
            }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
  