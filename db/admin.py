from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(Organization)
admin.site.register(AppDetail)
admin.site.register(Database)
admin.site.register(Plan)
admin.site.register(EnvDetail)