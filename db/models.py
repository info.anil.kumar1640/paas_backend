from django.db import models

# Create your models here.

REGION_CHOICES=(
    ('United State', 'United State'),
    ('India', 'India'),
    ('China', 'China')    
)

FRAMEWORK_CHOICES=(('Django','Django'),
                   ('Vue.js','Vue.js'),
                   ('Laravel', 'Laravel')
                   
                   )
PLAN_TYPE=(('startter', 'starter'),
           ('medium','medium'),
           ('advance','advance')
           )
OBJECT_OF =(('App', 'App'),('Database','Database'))



class Organization(models.Model):
    id=models.AutoField(primary_key=True)
    name = models.CharField(max_length=50,)
    repository = models.CharField(max_length=60)
    branch = models.CharField(max_length=60)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)


    def __str__(self):
        return str(self.name)
    
    class Meta:
        db_table = 'organizations'


class AppDetail(models.Model):
    id=models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    region = models.CharField(max_length=60, choices=REGION_CHOICES)
    framework = models.CharField(max_length=60, choices=FRAMEWORK_CHOICES)
    organization=models.ForeignKey(Organization, on_delete=models.PROTECT, related_name='app_detail')
    plan= models.ForeignKey('Plan', on_delete=models.PROTECT,limit_choices_to={'object_of':'App'},related_name='app_detail')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)


    def __str__(self) -> str:
        return str(self.name)


    class Meta:
        db_table='app_details'

class Plan(models.Model):
    id=models.AutoField(primary_key=True)
    plan_type = models.CharField(max_length=50, choices=PLAN_TYPE)
    object_of = models.CharField(max_length=20, choices=OBJECT_OF)
    storage = models.CharField(max_length=15)
    bandwidth = models.CharField(max_length=20)
    ram=models.CharField(max_length=20)
    monthly_cost=models.FloatField()
    hourly_cost=models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)


    def __str__(self) -> str:
        return str(self.plan_type)


    class Meta:
        db_table='plans'


class Database(models.Model):
    id=models.AutoField(primary_key=True)
    plan=models.ForeignKey(Plan, on_delete=models.PROTECT, limit_choices_to={'object_of':'Database'}, related_name='plan')
    app=models.ForeignKey(AppDetail, on_delete=models.PROTECT, related_name='app_detail')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)


    def __str__(self) -> str:
        return str(self.plan)

    class Meta:
        db_table='databases'

class EnvDetail(models.Model):
    id=models.AutoField(primary_key=True)
    key=models.CharField(max_length=30)
    value = models.CharField(max_length=200)
    app=models.ForeignKey(AppDetail, on_delete=models.PROTECT, related_name='app_details')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)


    def __str__(self) -> str:
        return str(self.key)

    class Meta:
        db_table='env_details'

